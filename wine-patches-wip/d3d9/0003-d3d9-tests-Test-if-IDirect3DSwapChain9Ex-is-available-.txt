From 90050f0385b835d1ed69fd0f7261b0f83991ac9a Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Michael=20M=C3=BCller?= <michael@fds-team.de>
Date: Mon, 9 Sep 2013 16:16:32 +0200
Subject: d3d9/tests: Test if IDirect3DSwapChain9Ex is available with
 IDirect3D9(Ex)

---
 dlls/d3d9/tests/d3d9ex.c |   38 ++++++++++++++++++++++++++++++++++++++
 1 file changed, 38 insertions(+)

diff --git a/dlls/d3d9/tests/d3d9ex.c b/dlls/d3d9/tests/d3d9ex.c
index 2deef76..c3f58e0 100644
--- a/dlls/d3d9/tests/d3d9ex.c
+++ b/dlls/d3d9/tests/d3d9ex.c
@@ -120,6 +120,8 @@ static void test_qi_base_to_ex(void)
     IDirect3D9Ex *d3d9ex = (void *) 0xdeadbeef;
     IDirect3DDevice9 *device;
     IDirect3DDevice9Ex *deviceEx = (void *) 0xdeadbeef;
+    IDirect3DSwapChain9 *swapchain = NULL;
+    IDirect3DSwapChain9Ex *swapchainEx = (void *)0xdeadbeef;
     HRESULT hr;
     HWND window = create_window();
     D3DPRESENT_PARAMETERS present_parameters;
@@ -158,6 +160,22 @@ static void test_qi_base_to_ex(void)
     ok(deviceEx == NULL, "QueryInterface returned interface %p, expected NULL\n", deviceEx);
     if(deviceEx) IDirect3DDevice9Ex_Release(deviceEx);
 
+    /* Get the implicit swapchain */
+    hr = IDirect3DDevice9_GetSwapChain(device, 0, &swapchain);
+    ok(SUCCEEDED(hr), "Failed to get the implicit swapchain (%08x).\n", hr);
+    if (SUCCEEDED(hr))
+    {
+        hr = IDirect3DSwapChain9_QueryInterface(swapchain, &IID_IDirect3DSwapChain9Ex, (void **)&swapchainEx);
+        ok(hr == E_NOINTERFACE,
+                "IDirect3DSwapChain9::QueryInterface for IID_IDirect3DSwapChain9Ex returned %08x, expected E_NOINTERFACE.\n",
+                hr);
+        ok(swapchainEx == NULL, "QueryInterface returned interface %p, expected NULL.\n", swapchainEx);
+        if (swapchainEx)
+            IDirect3DSwapChain9Ex_Release(swapchainEx);
+    }
+    if (swapchain)
+        IDirect3DSwapChain9_Release(swapchain);
+
     IDirect3DDevice9_Release(device);
 
 out:
@@ -171,6 +189,8 @@ static void test_qi_ex_to_base(void)
     IDirect3D9Ex *d3d9ex;
     IDirect3DDevice9 *device;
     IDirect3DDevice9Ex *deviceEx = (void *) 0xdeadbeef;
+    IDirect3DSwapChain9 *swapchain = NULL;
+    IDirect3DSwapChain9Ex *swapchainEx = (void *)0xdeadbeef;
     HRESULT hr;
     HWND window = create_window();
     D3DPRESENT_PARAMETERS present_parameters;
@@ -240,6 +260,24 @@ static void test_qi_ex_to_base(void)
     ok(ref == 2, "IDirect3DDevice9 refcount is %d, expected 2\n", ref);
     ref = getref((IUnknown *) deviceEx);
     ok(ref == 2, "IDirect3DDevice9Ex refcount is %d, expected 2\n", ref);
+
+    /* Get the implicit swapchain */
+    hr = IDirect3DDevice9_GetSwapChain(device, 0, &swapchain);
+    ok(SUCCEEDED(hr), "Failed to get the implicit swapchain (%08x).\n", hr);
+    if (SUCCEEDED(hr))
+    {
+        hr = IDirect3DSwapChain9_QueryInterface(swapchain, &IID_IDirect3DSwapChain9Ex, (void **)&swapchainEx);
+        ok(hr == D3D_OK,
+                "IDirect3DSwapChain9::QueryInterface for IID_IDirect3DSwapChain9Ex returned %08x, expected D3D_OK.\n",
+                hr);
+        ok(swapchainEx != NULL && swapchainEx != (void *)0xdeadbeef,
+                "QueryInterface returned interface %p, expected != NULL && != 0xdeadbeef.\n", swapchainEx);
+        if (swapchainEx)
+            IDirect3DSwapChain9Ex_Release(swapchainEx);
+    }
+    if (swapchain)
+        IDirect3DSwapChain9_Release(swapchain);
+
     if(deviceEx) IDirect3DDevice9Ex_Release(deviceEx);
     IDirect3DDevice9_Release(device);
 
-- 
1.7.9.5

