#ifndef ApiHook_h_
#define ApiHook_h_

bool installTimerHook();
bool handleTimerEvents();

#endif // ApiHook_h_